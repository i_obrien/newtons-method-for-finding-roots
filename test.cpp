// ian obrien
// CS451
// newtons's method to solve for roots of polynomials (for up to degree 99)
// currently only finds one root when given a real guess, working on functionality 
// to find a complex root if it exists or a real root (not sure which will output yet)
// until i get a full fledged newton method working where i can find all real and complex
// roots of a general polynomial in standard form.


#include <iostream>
#include <math.h>
#include <iomanip>
#include <complex>
using namespace std;
typedef complex<double> my_comp;


my_comp function(my_comp x, int coef[]);
my_comp derivative(my_comp x, int coef[]);

// definitions of 'constants'
#define ARRSZ 99
#define iterations 20 
#define DELTA 0.0000000001
#define precision 50

int main()
{
    int degree = 0;
    // my complex number storage devices using typedef
    my_comp x, fx, fprimex;
    double guess_real = 0, guess_img = 0;
    int user_coef = 0;
    int coef [ARRSZ];


    x = my_comp(1,3.5);
    cout << endl << endl << x << endl << endl;
    return 0;
}
