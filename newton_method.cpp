// ian obrien
// CS451
// newtons's method to solve for roots of polynomials (for up to degree 99)
// currently only finds one root when given a real guess, working on functionality 
// to find a complex root if it exists or a real root (not sure which will output yet)
// until i get a full fledged newton method working where i can find all real and complex
// roots of a general polynomial in standard form.


#include <iostream>
#include <math.h>
#include <iomanip>
#include <complex.h>
using namespace std;
typedef complex<double> comp_num;


comp_num function(comp_num x, int coef[]);
comp_num derivative(comp_num x, int coef[]);

// definitions of 'constants'
#define ARRSZ 99
#define iterations 20 
#define DELTA 0.0000000001
#define precision 50

int main()
{
    int degree = 0;
    comp_num x, fprimex, fx;
    double guess_real = 0, guess_img = 0;
    int user_coef = 0;
    int coef [ARRSZ];

    // initalize all values in array to zero
    for (int i = 0; i < (ARRSZ + 1); ++i)
    {
        coef[i] = 0;
    }
    
    // step 1 get degree of polynomial and get users guess
    cout << "\n\nwelcome to Ian's version of Newton's method\n\n";
    cout << "degree of polynomial (0-99): ";
    cin >> degree;
    cout << "initial guess of root in complex form (a + bi)";
    cout << endl << "guess for real part 'a': ";
    cin >> guess_real;
    cout << "guess for complex part 'b'i: ";
    cin >> guess_img;
    cout << endl << endl;
    x = (guess_real, guess_img);


    // loop to get desired coefficents for polynomial
    for (int i = 0; i < (degree+1); ++i)
    {
        cout << "enter coefficent for x^" << i << ": ";
        cin >> user_coef;
        coef[i] = user_coef;
    }
    
    // evaluate function at guess
    fx = function(x, coef);

    cout << "FUNCTION EVALUATED AT GUESS = " << fx << endl << endl;
    // evaluate function at derivative of function
    fprimex = derivative(x, coef);

    // apply NEWTONS METHOD to get approximation of a root
    for(int i = 0; i < iterations; ++i)
    {
	// if we are within accepted tolerance, take the answer as "sufficent"
        if(abs(function(x,coef)) <= abs(DELTA))
        {
            cout << endl << "WITHIN DELTA:" << "\t" << setprecision(precision) << x << endl;
            break;
        }
        x = x - ((function(x,coef))/derivative(x,coef));
        cout << i+1 << ":" << "\t" << setprecision(precision) << x << endl;
    }

    return 0;
}


//////////////////////////////////////////
//////////FUNCTION DEFINITIONS////////////
//////////////////////////////////////////



//evaluate the inital function at a guess
comp_num function(comp_num x, int coef [])
{
    comp_num fx;

    // define f(x) (include the constant!)
    for (int i = 0; i < (ARRSZ+1); ++i)
    {
        fx = fx + coef[i]*cpow(x,i);
    }
    return fx;
}

//evaluate function at it's derivative
comp_num  derivative(comp_num x, int coef[])
{
    comp_num fprimex = 0;

    for(int i = 1; i < (ARRSZ+1); ++i)
    {
        if(coef[i] != 0)
            fprimex = fprimex + i*coef[i]*cpow(x,(i-1));
    }
    return fprimex;
}
