// ian obrien
// CS451
// newtons's method to solve for roots of polynomials (for up to degree 99)


#include <iostream>
#include <math.h>
#include <iomanip>
#include <complex>
using namespace std;


double function(double guess, int coef[]);
double derivative(double guess, int coef[]);

// definitions of 'constants'
#define ARRSZ 99
#define iterations 20 
#define DELTA 0.0000000001
#define precision 50

int main()
{
    int degree = 0;
    double guess = 0, fx = 0, fprimex = 0;
    int user_coef = 0;
    int coef [ARRSZ];

    // initalize all values in array to zero
    for (int i = 0; i < (ARRSZ + 1); ++i)
    {
        coef[i] = 0;
    }
    
    // step 1 get degree of polynomial and get users guess
    cout << "\n\nwelcome to Ian's version of Newton's method\n\n";
    cout << "degree of polynomial (0-99): ";
    cin >> degree;
    cout << "initial guess of root: ";
    cin >> guess;
    cout << endl << endl;


    // loop to get desired coefficents for polynomial
    for (int i = 0; i < (degree+1); ++i)
    {
        cout << "enter coefficent for x^" << i << ": ";
        cin >> user_coef;
        coef[i] = user_coef;
    }
    
    // evaluate function at guess
    fx = function(guess, coef);

    cout << "FUNCTION EVALUATED AT GUESS = " << fx << endl << endl;
    // evaluate function at derivative of function
    fprimex = derivative(guess, coef);

    // apply NEWTONS METHOD to get approximation of a root
    for(int i = 0; i < iterations; ++i)
    {
	// if we are within accepted tolerance, take the answer as "sufficent"
        if(abs(function(guess,coef)) <= abs(DELTA))
        {
            cout << endl << "WITHIN DELTA:" << "\t" << setprecision(precision) << guess << endl;
            break;
        }
        guess = guess - ((function(guess,coef))/derivative(guess,coef));
        cout << i+1 << ":" << "\t" << setprecision(precision) << guess << endl;
    }

    return 0;
}


//////////////////////////////////////////
//////////FUNCTION DEFINITIONS////////////
//////////////////////////////////////////



//evaluate the inital function at a guess
double function(double guess, int coef [])
{
    double fx;

    // define f(x) (include the constant!)
    for (int i = 0; i < (ARRSZ+1); ++i)
    {
        fx = fx + (coef[i]*pow(guess,i));
    }
    return fx;
}

//evaluate function at it's derivative
double derivative(double guess, int coef[])
{
    double fprimex = 0;

    for(int i = 1; i < (ARRSZ+1); ++i)
    {
        if(coef[i] != 0)
            fprimex = fprimex + i*coef[i]*(pow(guess,(i-1)));
    }
    return fprimex;
}
